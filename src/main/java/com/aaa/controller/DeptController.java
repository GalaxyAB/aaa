package com.aaa.controller;

import com.aaa.entity.Result;
import com.aaa.service.Impl.DeptServiceImpl;
import com.aaa.vo.DeptVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/1 20:18
 * @description：
 * @modified By：
 */
@RestController
@RequestMapping("/dept")
public class DeptController {
    @Autowired
    private DeptServiceImpl deptService;

    @PostMapping("/queryAllDept/{current}/{pageSize}")
    public Result queryAllDept(@PathVariable(required = true) Integer current,
                               @PathVariable(required = true) Integer pageSize,
                               @RequestBody DeptVo deptVo){
        return deptService.queryAllDept(current, pageSize,deptVo);
    }


    /*@PostMapping("/addOrUpdateDept/{dname}/{loc}")
    public Result addOrUpdateDept(@PathVariable(required = true) String dname,
                                  @PathVariable(required = true) String loc){
        return deptService.addOrUpdateDept(dname,loc);
    }*/
    @PostMapping("/addOrUpdateDept/")
    public Result addOrUpdateDept(@RequestBody DeptVo deptVo){
        return deptService.addOrUpdateDept(deptVo);
    }
    @DeleteMapping("/deleteDeptById/{deptno}")
    public Result deleteDeptById(@PathVariable(required = true) Integer deptno){
        return deptService.deleteDeptById(deptno);
    }
}
