package com.aaa.controller;

import com.aaa.entity.Result;
import com.aaa.service.Impl.EmpServiceImpl;
import com.aaa.vo.DeptVo;
import com.aaa.vo.EmpVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/2 10:55
 * @description：
 * @modified By：
 */
@RestController
@RequestMapping("/emp")
public class EmpController {
    @Autowired
    private EmpServiceImpl empService;

    @PostMapping("/queryAllEmp/{current}/{pageSize}")
    public Result queryAllDept(@PathVariable(required = true) Integer current,
                               @PathVariable(required = true) Integer pageSize,
                               @RequestBody EmpVo empVo){
        return empService.queryAllEmp(current, pageSize,empVo);
    }

    @PostMapping("/addOrUpdateEmp/")
    public Result addOrUpdateEmp(@RequestBody EmpVo empVo){
        return empService.addOrUpdateEmp(empVo);
    }
}
