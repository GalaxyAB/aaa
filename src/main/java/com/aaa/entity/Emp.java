package com.aaa.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Emp)实体类
 *
 * @author makejava
 * @since 2023-02-01 20:19:38
 */
@Data
@TableName("emp")
@AllArgsConstructor
@NoArgsConstructor
public class Emp implements Serializable {
    private static final long serialVersionUID = 309943031023082063L;
    /**
     * 主键，员工编号
     */
    @TableId
    private Integer empno;
    
    private String ename;
    
    private String job;
    
    private Integer mgr;
    
    private Date hiredate;
    
    private String sal;
    
    private String comm;
    
    private Integer deptno;
    @TableField(exist = false)
    private String dname;
    @TableField(exist = false)
    private String loc;

    public Emp(String ename, String job, Integer mgr, Date hiredate, String sal, String comm, Integer deptno) {
        this.ename = ename;
        this.job = job;
        this.mgr = mgr;
        this.hiredate = hiredate;
        this.sal = sal;
        this.comm = comm;
        this.deptno = deptno;
    }
}

