package com.aaa.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Dept)实体类
 *
 * @author makejava
 * @since 2023-02-01 20:14:36
 */
@Data
@TableName("dept")
@AllArgsConstructor
@NoArgsConstructor
public class Dept implements Serializable {
    private static final long serialVersionUID = 554232936557445470L;
    @TableId
    private Integer deptno;

    private String dname;

    private String loc;

    public Dept(String dname, String loc) {
        this.dname = dname;
        this.loc = loc;
    }
}

