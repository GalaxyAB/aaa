package com.aaa.dao;

import com.aaa.entity.Emp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/2 14:07
 * @description：
 * @modified By：
 */
@Mapper
public interface EmpDao extends BaseMapper<Emp> {
}