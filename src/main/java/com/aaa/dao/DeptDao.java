package com.aaa.dao;

import com.aaa.entity.Dept;
import com.aaa.entity.Result;
import com.aaa.vo.DeptVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/1 20:20
 * @description：
 * @modified By：
 */
@Component
@Mapper
public interface DeptDao extends BaseMapper<Dept> {
    public void callP06(Map map);
}
