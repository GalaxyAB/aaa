package com.aaa.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/2 10:54
 * @description：
 * @modified By：
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmpVo {
    private Integer empno;

    private String ename;

    private String job;

    private Integer mgr;

    private Date hiredate;

    private String sal;

    private String comm;

    private Integer deptno;
}
