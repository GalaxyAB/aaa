package com.aaa.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/1 20:25
 * @description：
 * @modified By：
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeptVo {
    private Integer deptno;
    private String dname;
    private String loc;
}
