package com.aaa.service;

import com.aaa.entity.Result;
import com.aaa.vo.EmpVo;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/2 14:07
 * @description：
 * @modified By：
 */
public interface EmpService {
    Result queryAllEmp(Integer current, Integer pageSize, EmpVo empVo);

    Result addOrUpdateEmp(EmpVo empVo);
}