package com.aaa.service;

import com.aaa.entity.Result;
import com.aaa.vo.DeptVo;
import org.springframework.stereotype.Service;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/1 20:20
 * @description：
 * @modified By：
 */
public interface DpetService {
    Result queryAllDept(Integer current, Integer pageSize, DeptVo deptVo);

    Result addOrUpdateDept(DeptVo deptVo);
    /*Result addOrUpdateDept(String dname,String loc);*/
    Result deleteDeptById(Integer deptno);
}
