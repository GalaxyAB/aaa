package com.aaa.service.Impl;

import cn.hutool.core.util.ObjectUtil;
import com.aaa.dao.DeptDao;
import com.aaa.entity.Dept;
import com.aaa.entity.Result;
import com.aaa.service.DpetService;
import com.aaa.vo.DeptVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/1 20:20
 * @description：
 * @modified By：
 */
@Service
public class DeptServiceImpl implements DpetService {
    @Autowired
    private DeptDao deptDao;
    @Override
    public Result queryAllDept(Integer current, Integer pageSize,DeptVo  deptVo) {
        Page<Dept> page = new Page<>(current, pageSize);
        QueryWrapper<Dept> wrapper = new QueryWrapper<>();
        /*if(StringUtils.hasText(deptVo.getDname())){
            wrapper.like("dname", deptVo.getDname());
        }
        if(ObjectUtil.isNotEmpty(deptVo.getLoc())){
            wrapper.eq("loc",deptVo.getLoc());
        }*/

        Page<Dept> deptPage = deptDao.selectPage(page, wrapper);
        System.out.println("2222222222222222");
        System.out.println(deptPage.getRecords().size());
        if(ObjectUtil.isNotEmpty(deptPage)){
            return new Result<>(200,"查询成功",deptPage);
        }
        return new Result<>(500,"查询失败");
    }

    /*@Override
    public Result addOrUpdateDept(String dname, String loc) {
        Dept dept = new Dept(dname,loc);
        int insert = deptDao.insert(dept);
        if (insert>0){
            return new Result<>(200,"操作成功");
        }
        return new Result<>(500,"查询失败");
    }*/

    @Override
    public Result addOrUpdateDept(DeptVo deptVo) {
        if(ObjectUtil.isNotEmpty(deptVo.getDeptno())){

            Dept dept = new Dept();
            dept.setDeptno(deptVo.getDeptno());
            dept.setDname(deptVo.getDname());
            dept.setLoc(deptVo.getLoc());
            int i = deptDao.updateById(dept);
            if (i>0){
                return new Result<>(200,"操作成功");
            }
            return new Result<>(500,"查询失败");
        }else {
        Dept dept = new Dept(deptVo.getDname(),deptVo.getLoc());
        int insert = deptDao.insert(dept);
            if (insert>0){
                return new Result<>(200,"操作成功");
            }
        }
        return new Result<>(500,"查询失败");
    }

    @Override
    public Result deleteDeptById(Integer deptno) {
        Dept dept = new Dept();
        dept.setDeptno(deptno);
        int i = deptDao.deleteById(dept);
        if (i>0){
            return new Result<>(200,"操作成功");
        }
        return new Result<>(500,"查询失败");
    }
}
