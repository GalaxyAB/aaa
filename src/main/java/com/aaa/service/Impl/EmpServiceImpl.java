package com.aaa.service.Impl;

import cn.hutool.core.util.ObjectUtil;
import com.aaa.dao.DeptDao;
import com.aaa.dao.EmpDao;
import com.aaa.entity.Dept;
import com.aaa.entity.Emp;
import com.aaa.entity.Result;
import com.aaa.service.EmpService;
import com.aaa.vo.EmpVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：Huan
 * @date ：Created in 2023/2/2 14:08
 * @description：
 * @modified By：
 */
@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpDao empDao;
    @Autowired
    private DeptDao deptDao;

    @Override
    public Result queryAllEmp(Integer current, Integer pageSize, EmpVo empVo) {
        Page<Emp> page = new Page<>(current, pageSize);
        System.out.println(current);
        System.out.println(pageSize);
        Page<Emp> empPage = empDao.selectPage(page, null);
        List<Emp> records = empPage.getRecords();
        for (Emp record : records) {
            Integer deptno = record.getDeptno();
            QueryWrapper<Dept> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("deptno",record.getDeptno());
            Dept dept = deptDao.selectOne(wrapper1);
            record.setDname(dept.getDname());
            record.setLoc(dept.getLoc());
        }

        if(ObjectUtil.isNotEmpty(empPage)){
            return new Result<>(200,"查询成功",empPage);
        }
        return new Result<>(500,"查询失败");
    }

    @Override
    public Result addOrUpdateEmp(EmpVo empVo) {
        if (ObjectUtil.isEmpty(empVo.getEmpno())){
            Emp emp = new Emp(empVo.getEname(),null,null,null,"100","100",empVo.getDeptno());
            int insert = empDao.insert(emp);
            if (insert>0){
                return new Result<>(200,"操作成功");
            }

        }else{
            Emp emp = new Emp();
            emp.setEmpno(empVo.getEmpno());
            emp.setEname(empVo.getEname());
            emp.setDeptno(empVo.getDeptno());
            int i = empDao.updateById(emp);
            if (i>0){
                return new Result<>(200,"操作成功");
            }
        }
        return new Result<>(500,"查询失败");
    }
}
