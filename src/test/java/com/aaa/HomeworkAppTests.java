package com.aaa;

import com.aaa.dao.DeptDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class HomeworkAppTests {

    @Test
    void contextLoads() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/qy156?serverTimezone=Asia" +
                        "/Shanghai"
                , "root", "2022");
        CallableStatement call = connection.prepareCall("{call p04(?,?)}");
        call.setInt(1,10);
        call.registerOutParameter(2, Types.INTEGER);
        call.execute();
        int anInt = call.getInt(2);
        System.out.println(anInt);

    }

    @Autowired
    private DeptDao deptDao;
    @Test
    public void  test01(){
       Map map= new HashMap<>();
       map.put("n",10);
       map.put("sum",null);

       deptDao.callP06(map);
        System.out.println(map.get("sum"));
    }

}
